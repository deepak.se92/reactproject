import React from 'react';
import '../../App.css'

export default function Contact() {
    return (
        <div class="container">
            <div class="card">
            <div class="tabs is-centered">
  <ul>
    <li class="is-active"><a>Feedback Form</a></li>
    <li><a>About Us</a></li>
    <li><a>Contact</a></li>
    <li><a>Gallery</a></li>
  </ul>
</div>
  <div class="card-content">
    
  </div>
  <footer class="card-footer">
    <button class="button is-primary">Submit</button>
    <button class="button is-warning ">Reset</button>
  </footer>
</div>
        </div>
    )
}


